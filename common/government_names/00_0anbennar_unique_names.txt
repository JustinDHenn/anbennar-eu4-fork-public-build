# For specific combinations of culture, religion and other such triggers
# Will pick the first valid one it finds in list

#Heir and consort titles are generally kept simple for clarity unless there is something special in particular that can be used.

black_demense_magocracy = {
	rank = {
		1 = BLACK_DEMENSE
		2 = BLACK_DEMENSE
		3 = BLACK_DEMENSE
	}

	ruler_male = {
		1 = DARK_LORD
		2 = DARK_LORD
		3 = DARK_LORD
	}

	ruler_female = {
		1 = DARK_LADY
		2 = DARK_LADY
		3 = DARK_LADY
	}

	trigger = {
		tag = Z99
		government = theocracy
	}
}

counts_league = {
	rank = {
		1 = LEAGUE
		2 = KINGDOM
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = COUNT
		2 = KING
		3 = EMPEROR
	}

	ruler_female = {
		1 = COUNTESS
		2 = QUEEN
		3 = EMPRESS
	}
	consort_male  = {
		1 = CONSORT
		2 = KING_CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = PRINCESS	
		3 = PRINCESS
	}
	
	trigger = {
		tag = B55
	}
}

adventurer_iron_sceptre = {
	rank = {
		1 = ADVENTURER
	}
	
	ruler_male = {
		1 = MAGISTER
	}

	ruler_female = {
		1 = MAGISTRIX
	}
	consort_male  = {
		1 = CONSORT
	}
	
	consort_female = {
		1 = CONSORT
	}
	
	heir_male = {
		1 = MAGISTER
	}
	
	heir_female = {
		1 = MAGISTRIX
	}
	
	trigger = {
		tag = B20
		has_reform = adventurer_reform
	}
}

adventurer_sword_covenant = {
	rank = {
		1 = ADVENTURER
	}
	
	ruler_male = {
		1 = KNIGHT_CAPTAIN
	}

	ruler_female = {
		1 = KNIGHT_CAPTAIN
	}
	consort_male  = {
		1 = CONSORT
	}
	
	consort_female = {
		1 = CONSORT
	}
	
	heir_male = {
		1 = SER
	}
	
	heir_female = {
		1 = LADY
	}
	
	trigger = {
		tag = B19
		has_reform = adventurer_reform
	}
}

smallcountry_free_republic = {
	rank = {
		1 = REPUBLIC
		2 = GRAND_REPUBLIC
		3 = GREAT_REPUBLIC
	}

	ruler_male = {
		1 = FREEMAN
		2 = FREEMAN
		3 = FREEMAN
	}
	
	ruler_female = {
		1 = FREEWOMAN
		2 = FREEWOMAN
		3 = FREEWOMAN
	}

	trigger = {
		tag = A97
		government = republic
		#is_vassal = no
	}
}

tellum_republic = {
	rank = {
		1 = REPUBLIC
		2 = GRAND_REPUBLIC
		3 = GREAT_REPUBLIC
	}

	ruler_male = {
		1 = LORD_CRIER
		2 = LORD_CRIER
		3 = LORD_CRIER
	}
	
	ruler_female = {
		1 = LADY_CRIER
		2 = LADY_CRIER
		3 = LADY_CRIER
	}

	heir_male = {
		1 = YELLER_APPARENT
		2 = YELLER_APPARENT
		3 = YELLER_APPARENT
	}

	heir_female = {
		1 = YELLER_APPARENT
		2 = YELLER_APPARENT
		3 = YELLER_APPARENT
	}
	trigger = {
		tag = A86 #Tellum
		government = republic
	}
}

gnollish_viakkoc = {
	rank = {
		1 = PACK
		2 = KINGDOM
		3 = EMPIRE
	}
	
	ruler_male = {
		1 = PACK_LORD
		2 = CORSAIR_KING
		3 = SEA_SCOURGE
	}

	ruler_female = {
		1 = PACK_MISTRESS
		2 = CORSAIR_QUEEN
		3 = SEA_SCOURGE
	}
	consort_male  = {
		1 = CONSORT
		2 = KING_CONSORT
		3 = EMPEROR_CONSORT
	}
	
	consort_female = {
		1 = CONSORT
		2 = QUEEN_CONSORT
		3 = EMPRESS_CONSORT
	}
	
	heir_male = {
		1 = HEIR
		2 = PRINCE
		3 = PRINCE
	}
	
	heir_female = {
		1 = HEIR
		2 = PRINCESS	
		3 = PRINCESS
	}
	
	trigger = {
		culture_group = gnollish
		tag = F07
	}
}

skaldskola_theocracy = {
	rank = {
		1 = TEMPLE
		2 = GREAT_TEMPLE
		3 = HOLY_EMPIRE
	}

	ruler_male = {
		1 = MASTER_SKALD_PRIEST
		2 = MASTER_SKALD_PRIEST
		3 = MASTER_SKALD_PRIEST
	}

	ruler_female = {
		1 = MASTER_SKALD_PRIESTESS
		2 = MASTER_SKALD_PRIESTESS
		3 = MASTER_SKALD_PRIESTESS
	}

	trigger = {
		government = theocracy
		religion = skaldhyrric_faith
		tag = Z09
	}
}

cannorian_minaran_theocracy = {
	rank = {
		1 = TEMPLE
		2 = GREAT_TEMPLE
		3 = HOLY_EMPIRE
	}

	ruler_male = {
		1 = EXALTED_COMPANION
		2 = EXALTED_COMPANION
		3 = EXALTED_COMPANION
	}

	ruler_female = {
		1 = EXALTED_COMPANION
		2 = EXALTED_COMPANION
		3 = EXALTED_COMPANION
	}

	trigger = {
		government = theocracy
		tag = A10
	}
}

cannorian_dame_theocracy = {
	rank = {
		1 = TEMPLE
		2 = GREAT_TEMPLE
		3 = HOLY_EMPIRE
	}

	ruler_male = {
		1 = LUMINARY
		2 = HIGH_LUMINARY
		3 = LUMINARY
	}

	ruler_female = {
		1 = LUMINARY
		2 = HIGH_LUMINARY
		3 = HIGH_LUMINARY
	}

	trigger = {
		government = theocracy
		tag = A41
	}
}

eordand_magocracy = {
	rank = {
		1 = DRATHEAS
		2 = DRATHEAS
		3 = DRATHEAS
	}
	
	ruler_male = {
		1 = ARD_DRADH
		2 = ARD_DRADH
		3 = ARD_DRADH
	}
	
	ruler_female = {
		1 = ARD_BANNDRADH
		2 = ARD_BANNDRADH
		3 = ARD_BANNDRADH
	}
	
	heir_male = {
		1 = PRANT
		2 = PRANT
		3 = PRANT
	}
	
	heir_female = {
		1 = PRANT
		2 = PRANT
		3 = PRANT
	}
	
	trigger = {
		government = theocracy
		has_reform = magocracy_reform
		culture_group = eordellon_ruinborn_elf
	}
}
theocracy_mechanic = {
	has_devotion = yes
	rulers_can_be_generals = no
	heirs_can_be_generals = no
	royal_marriage = no
	different_religion_acceptance = -20
	different_religion_group_acceptance = -50
	religion = yes
	heir = yes
	basic_reform = yes # = invisible/does not take up a slot
	valid_for_nation_designer = no
	republican_name = no
}

#Leadership
leading_clergy_reform = {
	allow_normal_conversion = yes
	legacy_equivalent = theocratic_government
	icon = "religious_leader"
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	modifiers = {
		global_tax_modifier = 0.05
	}
}

monastic_order_reform = {
	modifiers = {
		fort_maintenance_modifier = -0.2
	}
	rulers_can_be_generals = yes
	heirs_can_be_generals = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "monks"
	allow_normal_conversion = yes
	legacy_equivalent = monastic_order_government
	monastic = yes
	fixed_rank = 1
	
	#new
	allow_migration = yes
}

papacy_reform = {
	potential = {
		tag = PAP
	}

	modifiers = {
		prestige_per_development_from_conversion = 0.33
		tolerance_own = 1
	}
	valid_for_nation_designer = no
	icon = "papacy"
	allow_normal_conversion = yes
	legacy_equivalent = papal_government
	papacy = yes
	allow_convert = no
	lock_level_when_selected = yes
	fixed_rank = 2

	custom_attributes = {
		locked_government_type = yes
	}
}

# Anbennar
secular_order_reform = {
	modifiers = {
		fort_maintenance_modifier = -0.2
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "soldiers"
	allow_normal_conversion = no
	legacy_equivalent = secular_order_government
	monastic = yes
	fixed_rank = 1
	
	different_religion_acceptance = 20
	different_religion_group_acceptance = 50
	rulers_can_be_generals = yes
	#allow_migration = yes
}

adventurer_reform = {
	potential = {
		has_reform = adventurer_reform	#aka you cant see it unless you're already one
	}
	modifiers = {
		manpower_recovery_speed = 0.2
		colonists = 1
		migration_cooldown = -0.5
		diplomats = -1
		diplomatic_upkeep = -2
		reform_progress_growth = -0.75
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "soldiers_2"
	allow_normal_conversion = no
	lock_level_when_selected = yes
	#republican_name = yes
	legacy_equivalent = adventurer
	monastic = yes
	has_devotion = yes
	#fixed_rank = 1
	
	different_religion_acceptance = 20
	different_religion_group_acceptance = 50
	rulers_can_be_generals = yes
	allow_migration = yes
	
	factions = {
		adv_marchers
		adv_pioneers
		adv_fortune_seekers
	}
	
	conditional = {
		allow = { has_dlc = "Rights of Man" }
		militarised_society = yes
	}
	
	custom_attributes = {
		locked_government_type = yes
	}
}

magisterium_reform = {
	potential = {
		tag = A85
	}

	modifiers = {	#for better spellcasting
		monarch_admin_power = 1
		monarch_diplomatic_power = 1
		monarch_military_power = 1
	}
	valid_for_nation_designer = no
	icon = "papacy"
	allow_normal_conversion = no
	legacy_equivalent = magisterium
	lock_level_when_selected = yes
	fixed_rank = 2
	
	rulers_can_be_generals = yes
	heirs_can_be_generals = yes

	custom_attributes = {
		locked_government_type = yes
	}
}

magocracy_reform = {
	potential = {
		OR = {
			has_estate = estate_mages
			has_country_flag = adventurer_derived_government
			}
		OR = {
			estate_influence = {
				estate = estate_mages
				influence = 20
			}
			culture_group = eordellon_ruinborn_elf
			has_country_flag = adventurer_derived_government
		}
	}

	modifiers = {	#for better spellcasting
		all_power_cost = -0.05
	}
	valid_for_nation_designer = no
	icon = "papacy"
	allow_normal_conversion = no
	legacy_equivalent = magocracy
	lock_level_when_selected = yes
	
	rulers_can_be_generals = yes
	heirs_can_be_generals = yes
}

rezankand_reform = { #Rezankand government
	icon = "papacy"
	allow_normal_conversion = no
	potential = {
		has_country_flag = rezankand_government
		OR = {
			tag = H67
		}
	}
	
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	nation_designer_trigger = {
	
	}
	
	legacy_equivalent = secular_order_government
	lock_level_when_selected = yes
	fixed_rank = 3
	maintain_dynasty = yes

	rulers_can_be_generals = yes
	heirs_can_be_generals = yes
	
	modifiers = {
		max_absolutism = 10
		institution_spread_from_true_faith = 0.15
		administrative_efficiency = 0.05
		num_accepted_cultures = 2
	}
	
	custom_attributes = {
		locked_government_type = yes
	}
}

#Internal vs External Mission

internal_mission_reform = {
	icon = "clergyman"
	allow_normal_conversion = yes
	modifiers = {
		tolerance_own = 2
	}
}

external_mission_reform = {
	icon = "soldiers"
	allow_normal_conversion = yes
	modifiers = {
		warscore_cost_vs_other_religion = -0.2
	}
}

#Divine Cause

safeguard_holy_sites_reform = {
	icon = "church"
	allow_normal_conversion = yes
	modifiers = {
		prestige = 1
	}
}

combat_heresy_reform = {
	icon = "soldiers_2"
	allow_normal_conversion = yes
	modifiers = {
		land_morale = 0.1
	}
}

expel_heathens_reform = {
	icon = "landscape"
	allow_normal_conversion = yes
	modifiers = {
		development_cost = -0.05
	}
}

#Clergy in Administration

subservient_administrators_reform = {
	icon = "paper_with_seal"
	allow_normal_conversion = yes
	modifiers = {
		free_policy = 1
	}
}

religious_administrators_reform = {
	icon = "paper_with_seal_3"
	allow_normal_conversion = yes
	modifiers = {
		stability_cost_modifier = -0.1
	}
}

#Secularization

maintain_religious_head_reform = {
	icon = "nobleman"
	allow_normal_conversion = yes
	modifiers = {
		yearly_absolutism = 0.1
	}
}

hereditary_religious_leadership_reform = {
	potential = {
		religion_group = muslim
	}
	icon = "muslim_highlighted"
	allow_normal_conversion = yes
	effect = {
		set_country_flag = populists_in_government
		change_government = monarchy
		add_government_reform = feudal_theocracy
	}
	ai = {
		factor = 0
	}
}

crown_leader_reform = {
	icon = "crown_highlighted"
	allow_normal_conversion = yes
	trigger = {
		NOT = { has_government_attribute_short_desc = locked_government_type }
	}
	effect = {
		set_country_flag = populists_in_government
		lose_reforms = 2
		change_government = monarchy
	}
	ai = {
		factor = 0
	}
}

proclaim_republic_reform = {
	icon = "parliament_highlighted"
	allow_normal_conversion = yes
	trigger = {
		NOT = { has_government_attribute_short_desc = locked_government_type }
	}
	effect = {
		set_country_flag = populists_in_government
		lose_reforms = 1
		change_government = republic
	}
	ai = {
		factor = 0
	}
}

battle_pope_reform = {
	icon = "soldiers_6"
	allow_normal_conversion = yes
	potential = {
		has_reform = papacy_reform
	}

	rulers_can_be_generals = yes

	modifiers = {
		leader_land_fire = 1
	}
}

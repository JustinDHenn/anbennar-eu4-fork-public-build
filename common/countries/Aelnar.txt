#Country Name: Please see filename.

graphical_culture = westerngfx

color = { 28 31 150 }

revolutionary_colors = { 153  239  222 }

historical_idea_groups = {
	exploration_ideas
	maritime_ideas
	quantity_ideas
	religious_ideas
	expansion_ideas
	quality_ideas	
	innovativeness_ideas
	offensive_ideas
}

historical_units = {
	western_medieval_infantry
	chevauchee
	western_men_at_arms
	swiss_landsknechten
	dutch_maurician
	french_caracolle
	anglofrench_line
	french_dragoon
	french_bluecoat
	french_cuirassier
	french_impulse
	mixed_order_infantry
	open_order_cavalry
	napoleonic_square
	napoleonic_lancers
}

monarch_names = {
	"Adrahel #0" = 1
	"Alarian #0" = 1
	"Aldarian #0" = 1
	"Aldarion #0" = 1
	"Alvarion #0" = 1
	"Andrellion #0" = 1
	"Aranthir #0" = 1
	"Ardor #0" = 1
	"Ardorian #0" = 1
	"Arfil #0" = 1
	"Artorian #0" = 1
	"Calasandal #0" = 1
	"Calasandur #0" = 1
	"Calindal #0" = 1
	"Calrodir #0" = 1
	"Camnar #0" = 1
	"Camnarian #0" = 1
	"Camnaril #0" = 1
	"Carodir #0" = 1
	"Celadil #0" = 1
	"Celador #0" = 1
	"Celadorian #0" = 1
	"Darandil #0" = 1
	"Darastarion #0" = 1
	"Denarion #0" = 1
	"Dorendor #0" = 1
	"Eborian #0" = 1
	"Elanil #0" = 1
	"Eledas #0" = 1
	"Elethar #0" = 1
	"Elethor #0" = 1
	"Elrian #0" = 1
	"Elriandor #0" = 1
	"Eranil #0" = 1
	"Erelas #0" = 1
	"Erendil #0" = 1
	"Erlanil #0" = 1
	"Evindal #0" = 1
	"Filinar #0" = 1
	"Finorian #0" = 1
	"Galinael #0" = 1
	"Galindil #0" = 1
	"Galindor #0" = 1
	"Gelehorn #0" = 1
	"Gelmonias #0" = 1
	"Ibenion #0" = 1
	"Ivran #0" = 1
	"Ivrandil #0" = 1
	"Ivrandir #0" = 1
	"Jahelor #0" = 1
	"Jaher #0" = 1
	"Munarion #0" = 1
	"Nesterin #0" = 1
	"Oloris #0" = 1
	"Pelodir #0" = 1
	"Serondal #0" = 1
	"Erondar #0" = 1
	"Serondor #0" = 1
	"Taelar #0" = 1
	"Taelarian #0" = 1
	"Taelarios #0" = 1
	"Thalanil #0" = 1
	"Thalanor #0" = 1
	"Thelrion #0" = 1
	"Thirendil #0" = 1
	"Thirendir #0" = 1
	"Threthinor #0" = 1
	"Triandil #0" = 1
	"Ultarion #0" = 1
	"Urion #0" = 1
	"Vaceran #0" = 1
	"Varamael #0" = 1
	"Varamar #0" = 1
	"Varamel #0" = 1
	"Varamelian #0" = 1
	"Varanar #0" = 1
	"Varilor #0" = 1
	"Vindal #0" = 1

	"Galindel #0" = -5
	"Istralania #0" = -5
	"Narawen #0" = -5
	"Erelassa #0" = -5
	"Filinara #0" = -5
	"Celadora #0" = -5
	"Thaliandel #0" = -5
	"Selussa #0" = -5
	"Imariel #0" = -5
	"Vehari #0" = -5
	"Veharia #0" = -5
	"Varila #0" = -5
	"Varilia #0" = -5
	"Panoril #0" = -5
	"Liandiel #0" = -5
	"Varinna #0" = -5
	"Ariathra #0" = -5
	"Ariathen #0" = -5
	"Amarien #0" = -5
	"Alarawel #0" = -5
	"Ladrindel #0" = -5
	"Leslindel #0" = -5
}

leader_names = {
	Warsinger Bladedancer Moondance Moonblade Sunspear Redblade Redsun Shadowweaver Shadowdancer Shadowblade Crimsonspear Seadancer Seamoon Seasail Doomdancer Stronghunter Whiteblade Whitedagger Brightdagger Brighteyes
	Silverhair Silvereyes Moonhair Mooneye Moonspirit Whisperwind Shadowsong Sunstrider Stormrage Feathermoon Ravencrest Stagwalker Forestwalker Gladewalker Gladehunter Gladedancer Gladeblade Bladewalker Moonwalker Moondancer
	Felsong Bloodsong Moonsong Firesong Seasong Goodsong Silversong Goldsong Windrunner Windwalker Windblade Moonrunner Sunspirit Winddancer Windmoon Windsail Sunchaser Moonstrider Bloodsworn Moonsworn Lovesworn Bowsworn
	Bowfeather Bowmaster Stringhair Bowhair Longhair Longbow Longsword Daggerdancer Sunreaver Sunfury Shadowfury Moonwatcher Elderdream Spellseer Spelldancer Spellblade Spelldagger Spellweaver Spellrunner Spellstrider
	Magehand Silverhand Silverbow Silverblade Silverfury Silverseer Silverspell Hornblade Stargazer Stardancer Starblade Starwatcher Starweaver Sunstar Moonstar Leafhunter Nightbranch Duskblade Duskspear Duskrunner Duskdancer
	Duskhand Skydancer Skyblade Skyseeker Skyfury Skyspinner Silverarrow Goldarrow Blackarrow Shadowarrow Skyarrow Magearrow Elderarrow Sunarrow Redarrow Bluearrow Gladearrow Woodarrow Whitearrow Brightarrow Bloodarrow
	Searrow Windarrow Truearrow Truesight Trueblade Truehair Truebow Truespell Trueseer Truehunter Truehand 
}

ship_names = {
	#Generic Cannorian
	Adamant Advantage Adventure Advice Answer Ardent Armada Arrogant Assistance Association Assurance Atlas Audacious 
	Bear Beaver "Black Galley" "Black Pinnace" "Black Prince" "Black Blood" Captain Centurion Coronation Courage Crocodile Crown
	Daisy Defence Defiance Devastation Diamond Director Dolphin Dragon Drake Dreadnaught Duke Duchess Eagle Elephant Excellent Exchange Expedition Experiment
	Falcon Fame Favourite Fellowship Fleetwood Flight Flirt Formidable Forrester Fortitude Fortune Gillyflower Globe "Golden Horse" "Golden Lion" "Golden Phoenix" Goliath Goodgrace Governor
	"Grand Mistress" "Great Bark" "Great Charity" "Great Galley" "Great Gift"  "Great Pinnace" "Great Zebra" "Green Dragon" Greyhound Griffin Guide "Half Moon" Hare Harpy Hawke Hazardous Heart
	Hero Hope "Hope Bark" "Hopeful Adventure" Humble Hunter Illustrious Impregnable Increase Indefatigable Inflexible Intrepid Invincible "Less Bark" "Less Pinnace" "Less Zebra"
	Lively Lizard Lion Magnanime Magnificent Majestic Makeshift Medusa Minotaur Moderate Monarch Moon Moor "New Bark" Ocean 
	Pansy Panther Parrot Porcupine Powerful President Prince "Prince Consort" "Prince Royal"
	Redoubtable Reformation Regent Renown Repulse Research Reserve Resistance Resolution Restoration Revenge 
	Salamander Sandwich Sapphire Satisfaction Seahorse Search Sheerness Speaker Speedwell Sphinx Splendid Sprite Stag Standard Stately Success Sunlight Sunbeam Superb Swan Sweepstake Swift Swiftsure
	Terrible Terror Thunderer Tiger Tremendous Trident Trinity Triumph Trusty
	Unicorn Union Unity Valiant Vanguard Venerable Vestal Veteran Victor Vindictive Virtue Violet
	Warrior Warspite "Young Prince" Zealous

	Survey Surveillance Cow Ox Bird Hawk Sovereign Emperor Count Lord Baron

	Knight Paladin Dragonslayer Cleric Rogue Fighter Ranger Sorcerer Wizard Warlock Monk Druid
	Strength Dexterity Constitution Intelligence Wisdom Charisma
}

army_names = {
	"Aelnar Army" "Elven Army" "Army of the Forest" "Army of the Isle" "Army of $PROVINCE$"
}

fleet_names = {
	"Aelnar Navy" "Elven Navy" "Westcoast Squadron" "Winebay Squadron" "Elven Squadron" "Moon Squadron" "Remnant Squadron" "Wanderer Squadron" "Seeker Squadron"
}
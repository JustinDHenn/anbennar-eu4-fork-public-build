government = monarchy
add_government_reform = autocracy_reform
government_rank = 1
primary_culture = peitar
religion = eordellon
technology_group = tech_eordand
capital = 2049
historical_friend = H06 #Sidpar

1433.3.5 = {
	monarch = {
		name = "Pelodan X"
		dynasty = "Oathsworn"
		birth_date = 1420.6.8
		adm = 2
		dip = 4
		mil = 6
	}
	add_ruler_personality = inspiring_leader_personality
	heir = {
		name = "Pelodan"
		monarch_name = "Pelodan XI"
		dynasty = "Oathsworn"
		birth_date = 1440.2.11
		death_date = 1481.3.2
		claim = 95
		adm = 3
		dip = 2
		mil = 4
	}
	queen = {
		country_of_origin = H06
		name = "Leslindel"
		dynasty = "Swiftsword"
		birth_date = 1419.5.20
		death_date = 1475.4.10
		female = yes
		adm = 2
		dip = 3
		mil = 3
	}
}

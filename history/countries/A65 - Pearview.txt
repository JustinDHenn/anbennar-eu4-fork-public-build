government = republic
add_government_reform = oligarchy_reform
government_rank = 1
primary_culture = redfoot_halfling
religion = regent_court
technology_group = tech_cannorian
national_focus = DIP
capital = 147
historical_rival = A64

1000.1.1 = { set_country_flag = mage_organization_decentralized_flag }

1440.2.2 = {
	monarch = {
		name = "Roderic II"
		dynasty = Peartree
		birth_date = 1423.9.3
		adm = 2
		dip = 4
		mil = 2
	}
}
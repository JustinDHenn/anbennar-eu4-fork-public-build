government = monarchy
add_government_reform = feudalism_reform
government_rank = 1
primary_culture = roilsardi
religion = regent_court
technology_group = tech_cannorian
capital = 59 # Roilsard
national_focus = DIP

1000.1.1 = { set_country_flag = mage_organization_magisterium_flag }
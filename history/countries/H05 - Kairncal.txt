government = theocracy
add_government_reform = monastic_order_reform
government_rank = 1
primary_culture = selphereg
religion = eordellon
technology_group = tech_eordand
capital = 2018

1430.3.10 = {
	monarch = {
		name = "Urion"
		dynasty = "Bluecrest"
		adm = 3
		dip = 5
		mil = 2
		birth_date = 1420.8.1
	}
	add_ruler_personality = navigator_personality
}
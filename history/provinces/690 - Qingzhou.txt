# No previous file for Qingzhou
owner = F29
controller = F29
add_core = F29
add_core = F25
culture = east_sandfang_gnoll
religion = xhazobkult

hre = no

base_tax = 1
base_production = 2
base_manpower = 1

trade_goods = wool

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_bulwari
#H�lsingland-G�strikland-Medelpad, from G�vle to N�s

owner = A41
controller = A41
add_core = A41
culture = east_damerian
religion = regent_court

hre = yes

base_tax = 4
base_production = 3
base_manpower = 3

trade_goods = damestear

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_gnollish

set_province_flag = permanent_damestear 
#268 - Moldavia

owner = A39
controller = A39
add_core = A39
culture = esmari
religion = regent_court

hre = yes

base_tax = 6
base_production = 5
base_manpower = 4

trade_goods = paper

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
#301 - Ryazan

owner = A37
controller = A37
add_core = A37
culture = esmari
religion = regent_court

hre = yes

base_tax = 4
base_production = 4
base_manpower = 3

trade_goods = grain

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish


#54 - Stade

owner = A31
controller = A31
add_core = A31
culture = moon_elf
religion = regent_court

hre = yes

base_tax = 4
base_production = 6
base_manpower = 3

trade_goods = fish

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
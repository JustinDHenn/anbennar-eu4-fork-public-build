# No previous file for Kherlen
owner = Z27
controller = Z27
add_core = Z27
culture = white_reachman
religion = regent_court

hre = no

base_tax = 6
base_production = 5
base_manpower = 4

trade_goods = naval_supplies

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_gnomish
discovered_by = tech_kobold
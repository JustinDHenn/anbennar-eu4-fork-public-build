# No previous file for Koch
owner = F24
controller = F24
add_core = F24
culture = brasanni
religion = bulwari_sun_cult

hre = no

base_tax = 2
base_production = 3
base_manpower = 1

trade_goods = salt

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari
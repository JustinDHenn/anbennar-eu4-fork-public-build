#375 - Munster

owner = A49
controller = A49
add_core = A49
culture = hill_gnoll
religion = xhazobkult

hre = no

base_tax = 1
base_production = 1
base_manpower = 1

trade_goods = wool

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_goblin
discovered_by = tech_bulwari
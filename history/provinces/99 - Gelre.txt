#100 - Gelre | 

owner = A09
controller = A09
add_core = A09
culture = sorncosti
religion = regent_court
hre = no
base_tax = 5
base_production = 5
trade_goods = wine
base_manpower = 3
capital = ""
is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

#15 - Western Jutland, incl. Ribe and Ringk�bing

owner = A04
controller = A04
add_core = A04
culture = west_damerian
religion = regent_court
hre = yes
base_tax = 7
base_production = 7
trade_goods = paper
center_of_trade = 1
base_manpower = 5
capital = ""
is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish

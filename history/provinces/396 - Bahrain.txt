#396 - Bahrain

owner = F07
controller = F07
add_core = F07
culture = west_sandfang_gnoll
religion = xhazobkult

hre = no

base_tax = 2
base_production = 3
base_manpower = 4

trade_goods = wool

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish

# No previous file for Erzurum
owner = A85
controller = A85
add_core = A85
culture = vernman
religion = regent_court

hre = yes

base_tax = 4
base_production = 6
base_manpower = 4

trade_goods = paper #scrolls and spells

capital = ""

is_city = yes
fort_15th = yes 

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish
#33 - Neva | 

owner = A16
controller = A16
add_core = A16
add_core = A87
add_core = A78
religion = regent_court
culture = roilsardi

hre = yes

base_tax = 8
base_production = 10
base_manpower = 3

trade_goods = wine
center_of_trade = 2
capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
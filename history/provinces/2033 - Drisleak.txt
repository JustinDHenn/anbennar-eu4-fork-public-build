# No previous file for Drisleak
owner = H04
controller = H04
add_core = H04
culture = peitar
religion = eordellon
capital = "Drisleak"

hre = no

base_tax = 2
base_production = 1
base_manpower = 1

trade_goods = fur

native_size = 14
native_ferocity = 6
native_hostileness = 6
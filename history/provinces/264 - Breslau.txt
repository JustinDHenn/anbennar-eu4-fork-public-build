#264 - Breslau (Breslau+Brieg+Öls+Wartenberg+Wohlau/Steinau)

owner = A95
controller = A95
add_core = A95
culture = esmari
religion = regent_court

hre = yes

base_tax = 4
base_production = 3
base_manpower = 4

trade_goods = cloth

capital = ""

is_city = yes


discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnomish
discovered_by = tech_orcish


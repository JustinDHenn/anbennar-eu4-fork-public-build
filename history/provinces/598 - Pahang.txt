# No previous file for Pahang
owner = F43
controller = F43
add_core = F43
culture = brasanni
religion = bulwari_sun_cult

hre = no

base_tax = 3
base_production = 3
base_manpower = 2

trade_goods = incense

capital = ""

is_city = yes

discovered_by = tech_cannorian
discovered_by = tech_elven
discovered_by = tech_dwarven
discovered_by = tech_salahadesi
discovered_by = tech_gnollish
discovered_by = tech_gnomish
discovered_by = tech_harpy
discovered_by = tech_goblin
discovered_by = tech_bulwari